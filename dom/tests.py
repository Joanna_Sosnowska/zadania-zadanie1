from dom.sum_client import client

__author__ = 'Joanna667'

import unittest
import socket

class EchoTestCase(unittest.TestCase):
    """testy dla klienta i serwera echo"""
    sending_msg = u'wysyłam "{0}"'
    received_msg = u'odebrano "{0}"'

    def test_partials(self):
        """Sprawdza sumowanie ułamkow z dużym, naprawdę dużym ogonem"""
        self.send_numbers(0.2341234556, 1.2345623456778)
    def test_large_numbers(self):
        """Sprawdza sumowanie dużych liczb"""
        self.send_numbers(2341234556, 2345623456778)

    def send_numbers(self, a,b):
        """Próba wysłania wiadomośći przy pomocy klienta.
        Raportowanie błędów związanych z gniazdem.
        """
        try:
            client(a, b)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Błąd: {0}, czy serwer działa?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail((u'Nieznany błąd: {0}'.format(str(e).decode('utf-8', 'replace'))).encode('utf-8'))


if __name__ == '__main__':
    unittest.main()

