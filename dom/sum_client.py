# -*- coding: utf-8 -*-
import socket
import sys
import logging
import logging.config



def client(m, n):
    adres = ('localhost', 5000)  # TODO: zmienić port i adres
    # Tworzenie gniazda TCP/IP
    gniazdo = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
    # Połączenie z gniazdem nasłuchującego serwera
    gniazdo.connect(adres)
    try:
        print("Serwer oblicza sumę liczb: " + str(m) + "+" + str(n))
        # przekazanie do serwera
        # uzywany jest łańcuch
        gniazdo.send((str(n) + "+" + str(m)+" ").encode('utf-8'))
        odp = ""
        data = gniazdo.recv(1)
        while data != b'':
            odp = odp + data.decode('utf-8')
            data = gniazdo.recv(1)

        print("Wynik = " + odp)
        pass

    finally:
        gniazdo.close()
        pass

if __name__ == '__main__':
        print("Podaj pierwszą liczbę:")
        a = sys.stdin.readline()
        print("Podaj drugą liczbę:")
        b = sys.stdin.readline()
        #żeby można było sumować także ułamki
        a1 = float(a)
        b1 = float(b)
        client(a1, b1)
