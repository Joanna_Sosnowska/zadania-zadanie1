# -*- coding: utf-8 -*-
import socket
import sys


def server():
    adres = ('localhost', 5000)  # TODO: zmienić port!
    gniazdo = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
    gniazdo.bind(adres)
    gniazdo.listen(1)
    try:
        while True:
            pol, ad = gniazdo.accept()
            temp = ""
            data = pol.recv(1)
            while data != b'+':
                temp = temp + data.decode('utf-8')
                data = pol.recv(1)
            a = float(temp)
            temp = ""
            data = pol.recv(1)
            while data != b' ':
                temp = temp + data.decode('utf-8')
                data = pol.recv(1)
            b = float(temp)
            try:
                pol.send(str(a + b).encode('utf-8'))
                pass
            finally:
                pol.close()
                pass

    except KeyboardInterrupt:
        pol.close()



if __name__ == '__main__':
    server()
    sys.exit(0)

