# -*- coding: utf-8 -*-

import socket
import unittest

from echo_client import client


class EchoTestCase(unittest.TestCase):
    """testy dla klienta i serwera echo"""
    sending_msg = u'wysyłam "{0}"'
    received_msg = u'odebrano "{0}"'

    def setUp(self):
        """ustawienia dla testów"""
        self.logger = LoggerStub()

    def test_short_message_echo(self):
        """Sprawdza przesyłanie krótkich wiadomości"""
        self.message_echo_test(u'krótka wiadomość')

    def test_long_message_echo(self):
        """Sprawdza przesyłanie długich wiadomości"""
        self.message_echo_test(u'''oto względnie długa wiadomość
        zaopatrzona w znaki nowych linii

        a nawet puste linie,
        które mogą siać spustoszenie wśród błędnie
        działającego kodu przetwarzającego dane z gniazd''')

    def message_echo_test(self, message):
        """Sprawdza przesyłanie wiadomości"""
        self.send_message(message)
        actual_sent, actual_received = self.process_log()
        expected_sent = self.sending_msg.format(message)
        self.assertEqual(expected_sent, actual_sent)

        expected_received = self.received_msg.format(message)
        self.assertEqual(expected_received, actual_received)

    def send_message(self, message):
        """Próba wysłania wiadomośći przy pomocy klienta.
        Raportowanie błędów związanych z gniazdem.
        """
        try:
            client(message, self.logger)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Błąd: {0}, czy serwer działa?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail((u'Nieznany błąd: {0}'.format(str(e).decode('utf-8', 'replace'))).encode('utf-8'))

    def process_log(self):
        """przetwarza dane wyłane do logera i zwraca komunikaty związane
        z wysłaną i otrzymaną wiadomością
        """
        if not self.logger.data:
            self.fail(u'Brak danych w loggerze')

        return self.logger.data[1], self.logger.data[2]


class LoggerStub:

    def __init__(self):
        self.data = []

    def info(self, msg):
        self.data.append(msg)


if __name__ == '__main__':
    unittest.main()